variable "project_id" {
  type        = "string"
  description = "The name of the project to instanciate the instance at."
  default     = "host-project-261007"
}

variable "project_region" {
  type        = "string"
  description = "Region where you want to deploy the resources."
  default     = "us-central1"
 }

variable "track3_sql_server1" {
  type        = "string"
  description = "1st Instance name."
  default     = "test-module"
}

variable "cred" {
  type        = "string"
  description = "Region where you want to deploy the resources."
  default     = ""
 }

variable "prefix" {
  type        = "string"
  description = "Region where you want to deploy the resources."
  default     = ""
 }